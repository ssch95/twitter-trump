package com.gitlab.ssch95.twitter.trump.loader.model;

import java.math.BigDecimal;

public class OutputTweet {
    private String id_str;
    private String text;
    private Long created_at_milliseconds;
    private Integer retweet_count;
    private String in_reply_to_user_id_str;
    private Integer favorite_count;
    private Boolean is_retweet;
    private String source;
    private String buzz;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getBuzz() {
        return buzz;
    }

    public void setBuzz(String buzz) {
        this.buzz = buzz;
    }


    public Long getCreated_at_milliseconds() {
        return created_at_milliseconds;
    }

    public void setCreated_at_milliseconds(Long created_at_milliseconds) {
        this.created_at_milliseconds = created_at_milliseconds;
    }

    public String getId_str() {
        return id_str;
    }

    public void setId_str(String id_str) {
        this.id_str = id_str;
    }

    public Long getCreated_at() {
        return created_at_milliseconds;
    }

    public void setCreated_at(Long created_at_milliseconds) {
        this.created_at_milliseconds = created_at_milliseconds;
    }

    public Integer getRetweet_count() {
        return retweet_count;
    }

    public void setRetweet_count(Integer retweet_count) {
        this.retweet_count = retweet_count;
    }

    public String getIn_reply_to_user_id_str() {
        return in_reply_to_user_id_str;
    }

    public void setIn_reply_to_user_id_str(String in_reply_to_user_id_str) {
        this.in_reply_to_user_id_str = in_reply_to_user_id_str;
    }

    public Boolean getIs_retweet() {
        return is_retweet;
    }

    public void setIs_retweet(Boolean is_retweet) {
        this.is_retweet = is_retweet;
    }

    public Integer getFavorite_count() {
        return favorite_count;
    }

    public void setFavorite_count(Integer favorite_count) {
        this.favorite_count = favorite_count;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
