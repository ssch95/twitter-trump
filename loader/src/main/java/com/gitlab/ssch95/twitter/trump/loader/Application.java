package com.gitlab.ssch95.twitter.trump.loader;

import com.gitlab.ssch95.twitter.trump.loader.model.InputTweet;
import com.gitlab.ssch95.twitter.trump.loader.model.OutputTweet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

    private static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();
    private static ZoneId twitterZoneId = ZoneId.of("America/New_York");
    private static DateTimeFormatter twitterDateTimeFormatter = DateTimeFormatter.ofPattern("EEE MMM d HH:mm:ss Z yyyy").localizedBy(Locale.ENGLISH).withZone(twitterZoneId);

    public static void main(String[] args) throws IOException {

        List<InputTweet> listOfTweets = concatListsOfTweets(
                readTweetsFromFile("2009.json"),
                readTweetsFromFile("2010.json"),
                readTweetsFromFile("2011.json"),
                readTweetsFromFile("2012.json"),
                readTweetsFromFile("2013.json"),
                readTweetsFromFile("2014.json"),
                readTweetsFromFile("2015.json"),
                readTweetsFromFile("2016.json"),
                readTweetsFromFile("2017.json"),
                readTweetsFromFile("2018.json")
        );

        saveTweets("china-tweets.csv", listOfTweets, "China");
        saveTweets("mexico-tweets.csv", listOfTweets, "Mexico","NAFTA","Border");
        saveTweets("canada-tweets.csv", listOfTweets, "Canada","NAFTA");
        saveTweets("russia-tweets.csv", listOfTweets, "Russia","Putin","Krim");
        saveTweets("tax-tweets.csv", listOfTweets, "tax");
        saveTweets("Illegal-Immigration-tweets.csv", listOfTweets, "Illegal","Immigration","Immigrants");
        saveTweets("trump-tweets.csv", listOfTweets);
  ;


    }

    private static List<InputTweet> readTweetsFromFile(String fileName) throws IOException {
        String json = Files.readString(Paths.get("input-data/tweets/" + fileName));
        return gson.fromJson(json, new TypeToken<List<InputTweet>>(){}.getType());
    }

    private static List<InputTweet> concatListsOfTweets(List<InputTweet>... listsOfTweets) {
        List<InputTweet> outputList = new ArrayList<>();
        for(List<InputTweet> listOfTweet: listsOfTweets) {
            outputList.addAll(listOfTweet);
        }
        return outputList;
    }

    private static void saveTweets(String fileName, List<InputTweet> tweets, String... keyWords) throws IOException {
        String csv = tweets.stream()
                .filter(tweet -> containsKeywords(tweet, keyWords))
                .filter(tweet -> tweetIsInTimeRange(tweet))
                .map(tweet -> mapTweet(tweet))
                .sorted(Comparator.comparing(tweet -> new BigDecimal(tweet.getBuzz())))
                .map(tweet -> mapToCsv(tweet))
                .collect(Collectors.joining("\n"));
        Files.writeString(Paths.get("filtered-data/" + fileName), csv);
    }


    private static String mapToCsv(OutputTweet outputTweet) {
        return "" +
                "\"" + outputTweet.getCreated_at_milliseconds() + "\"§" +
                "\"" + outputTweet.getBuzz() + "\"§" +
                "\"" + outputTweet.getFavorite_count() + "\"§" +
                "\"" + outputTweet.getRetweet_count() + "\"§" +
                "\"" + outputTweet.getId_str() + "\"§"+
                "\"" + outputTweet.getSource() + "\"§"+
                "\"" + outputTweet.getDate() + "\"§"+
                "\"" + outputTweet.getText().replaceAll("\n", " ") + "\"" ;
    }

    private static boolean containsKeywords(InputTweet tweet, String... keyWords) {
        return Stream.of(keyWords)
                .anyMatch(keyWord -> tweet.getText().contains(keyWord));
    }


    private static boolean tweetIsInTimeRange(InputTweet tweet) {
        return dateTimeOfTweet(tweet).isAfter(LocalDate.of(2016, 1, 1).atStartOfDay())
                && dateTimeOfTweet(tweet).isBefore(LocalDate.of(2017, 3, 8).atStartOfDay());
    }

    private static LocalDateTime dateTimeOfTweet(InputTweet inputTweet) {
        return ZonedDateTime.from(twitterDateTimeFormatter.parse(inputTweet.getCreated_at())).toLocalDateTime();
    }

    private static OutputTweet mapTweet(InputTweet inputTweet) {
        OutputTweet outputTweet = new OutputTweet();
        outputTweet.setCreated_at(ZonedDateTime.of(dateTimeOfTweet(inputTweet), twitterZoneId).toInstant().toEpochMilli());
        outputTweet.setFavorite_count(inputTweet.getFavorite_count());
        outputTweet.setId_str(inputTweet.getId_str());
        outputTweet.setIn_reply_to_user_id_str(inputTweet.getIn_reply_to_user_id_str());
        outputTweet.setIs_retweet(inputTweet.getIs_retweet());
        outputTweet.setRetweet_count(inputTweet.getRetweet_count());
        outputTweet.setText(inputTweet.getText());
        outputTweet.setSource(inputTweet.getSource());
        outputTweet.setBuzz(calculateBuzz(inputTweet));
        outputTweet.setDate(ZonedDateTime.of(dateTimeOfTweet(inputTweet), twitterZoneId).toLocalDate().toString());
        return outputTweet;
    }

    private static String calculateBuzz(InputTweet inputTweet) {
        MathContext mathContext = new MathContext(100, RoundingMode.HALF_UP);
        BigDecimal likes = BigDecimal.valueOf(inputTweet.getFavorite_count());
        BigDecimal likeFactor = new BigDecimal("0.3").multiply(likes, mathContext);
        BigDecimal retweets = BigDecimal.valueOf(inputTweet.getRetweet_count());
        BigDecimal retweetFactor = new BigDecimal("0.7").multiply(retweets, mathContext);
        return likeFactor.add(retweetFactor).toPlainString();
    }
}